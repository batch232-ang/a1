import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact myContact = new Contact("Luffy", "09111111111", "East Blue");
        Contact yourContact = new Contact("Sanji", "09222222222", "North Blue");

        phonebook.setContacts(myContact);
        phonebook.setContacts(yourContact);

        ArrayList<Contact> listOfContactObjects = phonebook.getContacts();

        if(listOfContactObjects.isEmpty()){
            System.out.println("List of Contacts is empty.");
        }else{
            for(Contact contact: listOfContactObjects){
                System.out.println(contact);
            }
        }


    }
}
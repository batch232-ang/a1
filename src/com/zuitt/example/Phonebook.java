package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook{
    private ArrayList<Contact> contacts = new ArrayList<>();
    private Contact contact = new Contact();

    public Phonebook(){};

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }
}

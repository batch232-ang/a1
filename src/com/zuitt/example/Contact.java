package com.zuitt.example;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact(){};

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName(){
        return this.name;
    }

    public String getContactNumber(){
        return this.contactNumber;
    }

    public String getAddress(){
        return this.address;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }

    @Override
    public String toString(){
        return this.name + " \n--------------\n" + this.name + " has the following registered number: \n" + this.contactNumber + "\n" + this.name + " has the following registered address:\nmy home in " + this.address + "\n";
    }
}
